# <h1 align='center'>Fitness Gym</h1>
## Frontend:
<div align='left'style='margin-bottom: 15px'>
  <img src='https://img.shields.io/badge/npm-5.6.0-1'>
  <img src='https://img.shields.io/badge/react-%5E18.0.0-green'>
  <img src='https://img.shields.io/badge/react--router--dom-%5E6.3.0-orange'>
  <img src='https://img.shields.io/badge/mobx-%5E6.5.0-yellow'>
  <img src='https://img.shields.io/badge/axios-%5E0.26.1-9cf'>
  <img src='https://img.shields.io/badge/framer--motion-%5E6.3.15-blue'>
</div>

## Backend:
<div align='left'style='margin-bottom: 15px'>
  <img src='https://img.shields.io/badge/npm-5.6.0-1'>
  <img src='https://img.shields.io/badge/node.js-%5E14.16.0-success'>
  <img src='https://img.shields.io/badge/express-%5E4.17.3-blue'>
  <img src='https://img.shields.io/badge/sequelize-%5E6.19.0-important'>
  <img src='https://img.shields.io/badge/jsonwebtoken-%5E8.5.1-blueviolet'>
  <img src='https://img.shields.io/badge/bcrypt-%5E5.0.1-ff69b4'>
</div>

![Typing SVG](https://readme-typing-svg.herokuapp.com?color=%2336BCF7&lines=Web+Application+"Fitness+Gym")


*This is a web application written in React, Node.js, PostgreSQL, Express.js. It is also my final qualifying work for the bachelor's degree. Client development, including drawing up a layout design in Figma, was implemented by me. Another person was responsible for the server part. This application was developed in June 2022 and was the first experience in developing a client-server architecture.*
___
# :link: [View Demo](http://45.84.224.151/)

## Features of the project

1. :white_check_mark: Registration and authorization via JWT token
2. :white_check_mark: Separation of roles into "client" and "administrator"
3. :white_check_mark: User's personal account
4. :white_check_mark: Admin panel (client management, class schedule management, handling requests from the contacts page)
5. :white_check_mark: FAQ section with the technique of performing exercises on simulators
6. :white_check_mark: Dynamic loading of information (search for clients in the admin panel)
___
## **Architecture of the project**
___

**Client Side**

![Client Side](https://i.ibb.co/4V5jXLQ/clientside.png)

**Server Side**

![Server Side](https://i.ibb.co/Mf6PsmJ/serverside.png)
___
## **Use Case Diagrams**
___
**Unathorized Client**

![Unathorized Client](https://i.ibb.co/LvrJbWL/unathorized.png)

**Authorized Client**

![Authorized Client](https://i.ibb.co/gSqVJPf/authorized-client.png)

**Authorized Administator**

![Authorized Administator](https://i.ibb.co/2c7rgL4/authorized-admin.png)

___
## **Some Screenshots**
___

**First Screen.**

![First Screen](https://i.ibb.co/sKjkzzT/mainscreen.png)

**Main Page.**

![Main Page](https://i.ibb.co/5jsztyT/image.png)

**/tickets route (Tickets Page).**

![/tickets route](https://i.ibb.co/QH9JxMH/image.png)
![/tickets route](https://i.ibb.co/G0qtyJd/image.png)

**/services route (Services Page).**

![/services route](https://i.ibb.co/mbwYPZs/image.png)
![/services route](https://i.ibb.co/2qyQB2X/image.png)

**/schedule route (Schedule Page).**

![/schedule route](https://i.ibb.co/wMX39rx/image.png)

**Registration form.**

![Registration Form](https://i.ibb.co/R4bpspS/image.png)

**Personal account (Profile).**

![Profile Page](https://i.ibb.co/d601fbp/image.png)

**/faq route (Guide Page).**

![Guide Page](https://i.ibb.co/2vQbS8B/image.png)
![Guide Page](https://i.ibb.co/N2Dcs6v/image.png)

**/admin route (Admin Page).**

![Admin Page](https://i.ibb.co/hcHWpmb/image.png)
![Admin Page](https://i.ibb.co/gwmB8Sg/image.png)
![Admin Page](https://i.ibb.co/P5WVwqJ/image.png)
![Admin Page](https://i.ibb.co/RCCZCxc/image.png)
