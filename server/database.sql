create TABLE users(
	id SERIAL PRIMARY KEY,
	name VARCHAR(255),
	email VARCHAR(255),
	password VARCHAR(255),
	date DATE,
	type VARCHAR(255),
	remained INT,
	trainer VARCHAR(255),
	role VARCHAR(50)
);

create TABLE cat_trainers(
	id SERIAL PRIMARY KEY,
	category VARCHAR(50)
);

-- create TABLE p_cat_trainers(
-- 	id SERIAL PRIMARY KEY,
-- 	p_category VARCHAR(50),
-- 	sort_order INT,
-- 	FOREIGN KEY (sort_order) REFERENCES cat_trainers (id)
-- 	ON DELETE CASCADE
-- );

create TABLE trainers(
	id SERIAL PRIMARY KEY,
	name VARCHAR(50),
	img VARCHAR(255),
	complexity INT,
	amount INT,
	mode VARCHAR(50),
	sub_des TEXT,
	target_num TEXT,
	action_num TEXT,
	description TEXT,
	rules_des TEXT,
	tech_num TEXT,
	tech_des TEXT,
	load_des TEXT,
	rules_num TEXT,
	categort_id INT,
	FOREIGN KEY (categort_id) REFERENCES cat_trainers (id)
	ON DELETE CASCADE
);